package pl.edu.pjatk.tau.selenium.pageobjects;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.By;

public class MainPage {

    String signLabelClassName = "login";

    public void clickSignInLabel(WebDriver driver) {
        driver.findElement(By.className(signLabelClassName)).click();
    }

    public String getTitle(WebDriver driver) {
        return driver.getTitle();
    }

    public boolean isAccountRegisterDisplayed(WebDriver driver, String accountName) {
        return driver.findElements(By.xpath("//a[@title='View my customer account']//span[contains(text(),'" + accountName + "')]")).size() > 0;
    }
}