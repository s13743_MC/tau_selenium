package pl.edu.pjatk.tau.selenium.stepdefs;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pl.edu.pjatk.tau.selenium.pageobjects.CreateAccountPage;
import pl.edu.pjatk.tau.selenium.pageobjects.LoginPage;
import pl.edu.pjatk.tau.selenium.pageobjects.MainPage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MainPageStepDefs {
    WebDriver driver;
    MainPage mainPage;
    LoginPage loginPage;
    CreateAccountPage createAccountPage;

    @Given("^Open the Firefox and enter http://automationpractice.com$")
    public void firefoxIsLaunched() throws Exception {
        driver = new FirefoxDriver();
        String baseUrl = "http://automationpractice.com";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);
        mainPage = new MainPage();
        loginPage = new LoginPage();
        createAccountPage = new CreateAccountPage();
    }

    @When("^I click sign in label$")
    public void iClickSignInLabel() throws Exception {
        mainPage.clickSignInLabel(driver);
    }

    @Then("^I am on login page$")
    public void iAmOnLoginPage() throws Exception {
        mainPage = new MainPage();
        String expectedTitle = "Login - My Store";
        String actualTitle = mainPage.getTitle(driver);
        assertEquals(expectedTitle, actualTitle);
    }

    @When("^I click Create an Account button")
    public void iClickCreateAnAccountButtonWithEmptyInput() throws Exception {
        loginPage = new LoginPage();
        loginPage.clickButtonCreateAnAccount(driver);
    }

    @Then("^Error message is displayed$")
    public void errorMessageIsDisplayed() throws Exception {
        loginPage = new LoginPage();
        assertTrue(loginPage.errorMessageDisplayed(driver));
    }

    @Given("^I am input \"([^\"]*)\" email address for create new account$")
    public void iAmInputEmailAddressForCreateNewAccount(String email) throws Exception {
        loginPage.inputEmailAddress(driver, email);
    }

    @Then("^I am on Create an Account page$")
    public void iAmOnCreateAnAccountPage() throws Exception {
        createAccountPage.textForCreateAccountPageIsDisplayed(driver);
    }

    @Given("^I click Register button")
    public void iClickRegisterButtonWithEmptyRegisterForm() throws Exception {
        createAccountPage.clickRegisterButton(driver);
    }

    @Then("^Error message is displayed on Register form$")
    public void errorMessageIsDisplayedOnRegisterForm() throws Exception {
        assertTrue(createAccountPage.textForClickEmptyRegisterErrorIsDisplayed(driver));
    }

    @Given("^I input \"([^\"]*)\" in First Name input$")
    public void iInputInFirstNameInput(String firstName) throws Exception {
        createAccountPage.inputFirstNameOnPersonalInformation(driver, firstName);
    }

    @Then("^Input First Name is highlighted on red color$")
    public void inputFirstNameIsHighlightedOnRedColor() throws Exception {
        createAccountPage.firstNameInputOnPersonalInformationIsHighlightedRed(driver);
    }

    @Then("^I see \"([^\"]*)\" in email address input$")
    public void iSeeInEmailAddressInput(String email) throws Exception {
        createAccountPage.isEmailAddressInputed(driver, email);
    }

    @Given("^I input \"([^\"]*)\" in Last Name input$")
    public void iInputInLastNameInput(String lastName) throws Exception {
        createAccountPage.inputLastNameOnPersonalInformation(driver, lastName);
    }

    @Given("^I input \"([^\"]*)\" in Password input$")
    public void iInputInPasswordInput(String password) throws Exception {
        createAccountPage.inputPassword(driver, password);
    }

    @Given("^I input \"([^\"]*)\" in Mobile phone input$")
    public void iInputInMobilePhoneInput(String number) throws Exception {
        createAccountPage.inputMobilePhoneNumber(driver, number);
    }

    @Given("^I input \"([^\"]*)\" in City input$")
    public void iInputInCityInput(String city) throws Exception {
        createAccountPage.inputCity(driver, city);
    }

    @Given("^I input \"([^\"]*)\" in Address input$")
    public void iInputInAddressInput(String address) throws Exception {
        createAccountPage.inputAddress(driver, address);
    }

    @Given("^I input \"([^\"]*)\" in Zip Code input$")
    public void iInputInZipCodeInput(String zipCode) throws Exception {
        createAccountPage.inputZipCode(driver, zipCode);
    }

    @Given("^I select \"([^\"]*)\" from Sate drop-down menu$")
    public void iSelectFromSateDropDownMenu(String state) throws Exception {
        createAccountPage.selectState(driver, state);
    }

    @Then("^I am logged in as \"([^\"]*)\"$")
    public void iAmLoggedInAs(String firstAndLastName) throws Exception {
        assertTrue(mainPage.isAccountRegisterDisplayed(driver, firstAndLastName));
    }

    @After
    public void cleanUp() {
        driver.quit();
    }
}