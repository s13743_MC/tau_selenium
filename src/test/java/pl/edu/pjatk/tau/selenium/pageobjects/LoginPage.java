package pl.edu.pjatk.tau.selenium.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    private String buttonCreateAnAccountLocatorId = "SubmitCreate";
    private String fieldCreateAccountErrorLocatorId = "create_account_error";
    private String inputEmailAddressLocatorId = "email_create";

    public void clickButtonCreateAnAccount(WebDriver driver) {
        driver.findElement(By.id(buttonCreateAnAccountLocatorId)).click();
    }

    public boolean errorMessageDisplayed(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement fieldCreateAccountError = driver.findElement(By.id(fieldCreateAccountErrorLocatorId));
        WebElement element = wait.until(ExpectedConditions.visibilityOf(fieldCreateAccountError));
        return element.isDisplayed();
    }

    public void inputEmailAddress(WebDriver driver, String email) {
        driver.findElement(By.id(inputEmailAddressLocatorId)).sendKeys(email);
    }
}