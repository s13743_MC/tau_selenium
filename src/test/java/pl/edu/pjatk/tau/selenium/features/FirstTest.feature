Feature: First test

    Scenario: Validation Registration Form
      Given Open the Firefox and enter http://automationpractice.com
      When I click sign in label
      Then I am on login page

#    Scenario: Click "Create an Account" button with empty input
      Given I am on login page
      When I click Create an Account button with empty input
      Then Error message is displayed

      Given I am input "jarobot100@pjwstk.edu.pl" email address for create new account
      When I click Create an Account button
      Then I am on Create an Account page
      And I see "jarobot100@pjwstk.edu.pl" in email address input

      Given I click Register button with empty register form
      Then Error message is displayed on Register form

      Given I input "Mikolaj%" in First Name input
      Then Input First Name is highlighted on red color

      Given I input "Mikolaj" in First Name input
      And I input "Chodkowski" in Last Name input
      And I input "mikolaj1" in Password input
      And I input "663033104" in Mobile phone input
      And I input "Gdansk" in City input
      And I input "Lema 12" in Address input
      And I input "80126" in Zip Code input
      And I select "Alabama" from Sate drop-down menu
      When I click Register button
      Then I am logged in as "Mikolaj Chodkowski"