package pl.edu.pjatk.tau.selenium;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"},
        features = "src/test/java/pl/edu/pjatk/tau/selenium/features",
        snippets = SnippetType.CAMELCASE)
public class RunCucumberTest {
}