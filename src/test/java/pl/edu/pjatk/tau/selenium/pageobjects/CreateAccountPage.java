package pl.edu.pjatk.tau.selenium.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateAccountPage {

    private String createAccountPageIdentifier = "Assign an address alias for future reference.";
    private String buttonRegisterLocatorId = "submitAccount";
    private String fieldRegisterErrorLocatorText = "There are 8 errors";
    private String inputFirstNamePersonalInformationId = "customer_firstname";
    private String inputLastNamePersonalInformationId = "customer_lastname";
    private String inputEmailLocatorId = "email";
    private String inputFirtNamePersonalInformationErrorXpath = "//div[@class='required form-group form-error']//input[@id='customer_firstname']";
    private String inputPasswordLocatorId = "passwd";
    private String inputMobilePhoneNumber = "phone_mobile";
    private String inputCity = "city";
    private String inputAddress = "address1";
    private String inputZipCode = "postcode";

    public boolean textForCreateAccountPageIsDisplayed(WebDriver driver) {
        return this.isTextDisplayedOnRegisterPage(driver, createAccountPageIdentifier);
    }

    public void clickRegisterButton(WebDriver driver) {
        driver.findElement(By.id(buttonRegisterLocatorId)).click();
    }

    public boolean textForClickEmptyRegisterErrorIsDisplayed(WebDriver driver) {
        return this.isTextDisplayedOnRegisterPage(driver, fieldRegisterErrorLocatorText);
    }

    public void inputFirstNameOnPersonalInformation(WebDriver driver, String firstName) {
        driver.findElement(By.id(inputFirstNamePersonalInformationId)).clear();
        driver.findElement(By.id(inputFirstNamePersonalInformationId)).sendKeys(firstName);
        driver.findElement(By.id(inputLastNamePersonalInformationId)).sendKeys("");
    }

    public boolean firstNameInputOnPersonalInformationIsHighlightedRed(WebDriver driver) {
        return driver.findElements(By.xpath(inputFirtNamePersonalInformationErrorXpath)).size() > 0;
    }

    public boolean isEmailAddressInputed(WebDriver driver, String email) {
        return driver.findElement(By.id(inputEmailLocatorId)).getAttribute("value").equals(email);
    }

    public void inputLastNameOnPersonalInformation(WebDriver driver, String lastName) {
        driver.findElement(By.id(inputLastNamePersonalInformationId)).sendKeys(lastName);
    }

    public void inputPassword(WebDriver driver, String password) {
        driver.findElement(By.id(inputPasswordLocatorId)).sendKeys(password);
    }

    public void inputMobilePhoneNumber(WebDriver driver, String number) {
        driver.findElement(By.id(inputMobilePhoneNumber)).sendKeys(number);
    }

    public void inputCity(WebDriver driver, String city) {
        driver.findElement(By.id(inputCity)).sendKeys(city);
    }

    public void inputAddress(WebDriver driver, String address) {
        driver.findElement(By.id(inputAddress)).sendKeys(address);
    }

    public void inputZipCode(WebDriver driver, String zipCode) {
        driver.findElement(By.id(inputZipCode)).sendKeys(zipCode);
    }

    public void selectState(WebDriver driver, String state) {

        Select stateSelect = new Select(driver.findElement(By.id("id_state")));
        stateSelect.selectByVisibleText(state);
    }

    // common methods part
    private boolean isTextDisplayedOnRegisterPage(WebDriver driver, String text) {
        return driver.findElements(By.xpath("//*[contains(text(), '" + text + "')]")).size() > 0;
    }
}